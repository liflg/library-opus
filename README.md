Website
=======
http://www.opus-codec.org/

License
=======
three-clause BSD license (see the file source/COPYING)

Version
=======
1.3

Source
======
opus-1.3.tar.gz (sha256: 4f3d69aefdf2dbaf9825408e452a8a414ffc60494c70633560700398820dc550)

Required by
===========
* opusfile
